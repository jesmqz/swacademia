<?php
    function getDataConnection() {
        if (!is_readable("config.env")) {
            return null;
        }
        $gestor = fopen("config.env", "r");
        $conexiondb = ["attr" => []];
        $i = 0;
        while($linea = fgets($gestor)) {
            $cleanline = trim($linea);
            $conexiondb["attr"][$i] = $cleanline;
            $i++;
        }
        fclose($gestor);
        
        return ($conexiondb);
    }
    
    function getLastPeriodo() {
        $lastPeriodo = "2019-01";
        $data = array ("status" => "success", "periodo" => $lastPeriodo);
        return $data;
        
        $conexiondb = getDataConnection();
        if ($conexiondb == null) {
            $data = array ("status" => "failed", "ERROR" => "archivo ENV DB no existe");
            return $data;
        }
        
        $conn = oci_connect($conexiondb["attr"][0], $conexiondb["attr"][1], $conexiondb["attr"][2]);

        if (!$conn) {
            $e = oci_error();
            // trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
            $data = array ("status" => "failed", "ERROR" => $e['message']);
            return $data;
        }else{
            $querysql3 = 'SELECT * FROM (SELECT DISTINCT PERIODO FROM UAOWEB.V_MOODLE_CURSO_GRUPO ORDER BY PERIODO DESC) WHERE ROWNUM = 1';
            $stid3 = oci_parse($conn, $querysql3);
            oci_execute($stid3);
            
            $sw = false;
            while (($row = oci_fetch_array($stid3, OCI_ASSOC)) != false) {
                // printf("Periodo:%s\n", $row['PERIODO']);
                $lastPeriodo = $row['PERIODO'];
                $sw = true;
            }
            oci_free_statement($stid3);
            oci_close($conn);
            $lastPeriodo = '2018-03';
            if ($sw)
                $data = array ("status" => "success", "periodo" => $lastPeriodo);
            else
                $data = array ("status" => "not-found", "periodo" => "no hay registros");
            
            return $data;
        }
    }
    
    function getPeriodos() {
        $conexiondb = getDataConnection();
        if ($conexiondb == null) {
            $data = array ("status" => "failed", "ERROR" => "archivo ENV DB no existe");
            return $data;
        }
        
        $conn = oci_connect($conexiondb["attr"][0], $conexiondb["attr"][1], $conexiondb["attr"][2]);

        if (!$conn) {
            $e = oci_error();
            // trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
            $data = array ("status" => "failed", "ERROR" => $e['message']);
            return $data;
        }else{
            // query
            $querysql3 = 'SELECT DISTINCT PERIODO FROM UAOWEB.V_MOODLE_CURSO_GRUPO ORDER BY PERIODO';
            $stid3 = oci_parse($conn, $querysql3);
            oci_execute($stid3);
            
            // procesamiento
            $sw = false;
            $periodos = array();
            
            while (($row = oci_fetch_array($stid3, OCI_ASSOC)) != false) {
                $periodos[] = $row['PERIODO'];
                $sw = true;
            }            
            
            oci_free_statement($stid3);
            oci_close($conn);

            if ($sw)
                $data = array ("status" => "success", "periodos" => $periodos);
            else
                $data = array ("status" => "not-found", "periodos" => "no hay registros");
            
            return $data;
        }

    }
    
    function getDocente($username) {
        $conexiondb = getDataConnection();
        if ($conexiondb == null) {
            $data = array ("status" => "failed", "ERROR" => "archivo ENV DB no existe");
            return $data;
        }
        
        $conn = oci_connect($conexiondb["attr"][0], $conexiondb["attr"][1], $conexiondb["attr"][2]);

        if (!$conn) {
            $e = oci_error();
            // trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
            $data = array ("status" => "failed", "ERROR" => $e['message']);
            return $data;
        }else{
            $querysql = 'SELECT * FROM UAOWEB.V_MOODLE_DOCENTES WHERE USERNAME = :usuariodoc';
            $stid = oci_parse($conn, $querysql);

            oci_bind_by_name($stid, ':usuariodoc', $username);
            oci_execute($stid);

            $found_docente = false;
            while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                $docente = array("username" => $row['USERNAME'],
                    "firstname" => $row['FIRSTNAME'],
                    "lastname"  => $row['LASTNAME']);
                    
                $found_docente = true;
            }
            
            if ($found_docente) {
                $data = array("status" => "success", 
                                "username" => $docente["username"], 
                                "firstname" => $docente["firstname"],
                                "lastname" => $docente["lastname"]);
            } else {
                $data = array("status" => "not-found",
                                "message" => "docente no encontrado");
            }
            oci_free_statement($stid);
            oci_close($conn);
            return $data;
        }
    }

    function getAsignatura($codigo) {
        $conexiondb = getDataConnection();
        if ($conexiondb == null) {
            $data = array ("status" => "failed", "ERROR" => "archivo ENV DB no existe");
            return $data;
        }
        
        $conn = oci_connect($conexiondb["attr"][0], $conexiondb["attr"][1], $conexiondb["attr"][2]);

        if (!$conn) {
            $e = oci_error();
            // trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
            $data = array ("status" => "failed", "ERROR" => $e['message']);
            return $data;
        }else{
            $dataPeriodo = getLastPeriodo();

            if ($dataPeriodo["status"] == "success") {
                $querysql = 'SELECT * FROM UAOWEB.V_MOODLE_ASIGNATURA WHERE CODIGO_ASIGNATURA = :cod_asignatura AND PERIODO = :periodo';
                $stid = oci_parse($conn, $querysql);

                oci_bind_by_name($stid, ':cod_asignatura', $codigo);
                oci_bind_by_name($stid, ':periodo', $dataPeriodo["periodo"]);
                oci_execute($stid);
                
                $found_asignatura = false;

                while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
                    $asignatura = array("codigo" => $row['CODIGO_ASIGNATURA'],
                            "nombre" => $row['NOMBRE_ASIGNATURA'],
                            "facultad"  => $row['FACULTAD'],
                            "departamento" => $row['DEPARTAMENTO']);
                    $found_asignatura = true;
                }

                if ($found_asignatura) {
                    $data = array("status" => "success", 
                                    "codigo" => $asignatura["codigo"], 
                                    "nombre" => $asignatura["nombre"],
                                    "facultad" => $asignatura["facultad"],
                                    "departamento" => $asignatura["departamento"]);
                } else {
                    $data = array("status" => "not-found",
                                    "message" => "asignatura no encontrada");
                }

                oci_free_statement($stid);
                oci_close($conn);
                return $data;
            }
            else {
                $data = array ("status" => "not-found", "periodo" => "no hay registros");
                return $data;
            }
        }
    }

    function getGrupos($codigo_asignatura, $periodo) {
        $conexiondb = getDataConnection();
        if ($conexiondb == null) {
            $data = array ("status" => "failed", "ERROR" => "archivo ENV DB no existe");
            return $data;
        }
        
        $conn = oci_connect($conexiondb["attr"][0], $conexiondb["attr"][1], $conexiondb["attr"][2]);

        if (!$conn) {
            $e = oci_error();
            // trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
            $data = array ("status" => "failed", "ERROR" => $e['message']);
            return $data;
        }else{
            $querysql2 = 'SELECT * FROM UAOWEB.V_MOODLE_CURSO_GRUPO WHERE PERIODO = :periodo AND CODIGO_CURSO LIKE :cod_curso';
            $stid2 = oci_parse($conn, $querysql2);

            $like_curso = $codigo_asignatura."%";
            
            oci_bind_by_name($stid2, ':periodo', $periodo);
            oci_bind_by_name($stid2, ':cod_curso', $like_curso);
            oci_execute($stid2);
            
            $grupos = array();
            while (($row = oci_fetch_array($stid2, OCI_ASSOC)) != false) {
                $grupos[] = $row['GRUPO'];
            }

            oci_free_statement($stid2);
            oci_close($conn);

            $data = array("status" => "success", "grupos" => $grupos);
            return $data;
        }
    }

    function getEstudiantes($curso, $periodo) {
        $conexiondb = getDataConnection();
        if ($conexiondb == null) {
            $data = array ("status" => "failed", "ERROR" => "archivo ENV DB no existe");
            return $data;
        }
        
        $conn = oci_connect($conexiondb["attr"][0], $conexiondb["attr"][1], $conexiondb["attr"][2]);

        if (!$conn) {
            $e = oci_error();
            // trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
            $data = array ("status" => "failed", "ERROR" => $e['message']);
            return $data;
        }else{
			$querysql = 'SELECT * FROM UAOWEB.V_MOODLE_CURSO_GRUPO_EST WHERE PERIODO = :periodo AND CODIGO_CURSO = :curso';
			//$querysql = 'SELECT * FROM UAOWEB.V_MOODLE_CURSO_GRUPO_EST WHERE PERIODO = :periodo AND GRUPO = :curso';
			$stid = oci_parse($conn, $querysql);
			oci_bind_by_name($stid, ':periodo', $periodo);
			oci_bind_by_name($stid, ':curso', $curso);
			oci_execute($stid);
			
			$estudiantes = array();
			$sw = false;
			while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
				$estudiantes[] = array($row['USUARIO'], $row['COD_PROG'], $row['CICLO']);
				$sw = true;
			}

			oci_free_statement($stid);
			oci_close($conn);
			
			if ($sw) {
				$data = array("status" => "success", "estudiantes" => $estudiantes);
				
				return $data;
			} else {
				$data = array ("status" => "not-found", "msg" => "no hay registros curso: ".$curso." periodo: ".$periodo);
                return $data;
			}
        }
    }
	
	function getProgramas($periodo) {
		$conexiondb = getDataConnection();
        if ($conexiondb == null) {
            $data = array ("status" => "failed", "ERROR" => "archivo ENV DB no existe");
            return $data;
        }
        
        $conn = oci_connect($conexiondb["attr"][0], $conexiondb["attr"][1], $conexiondb["attr"][2]);

        if (!$conn) {
            $e = oci_error();
            // trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
            $data = array ("status" => "failed", "ERROR" => $e['message']);
            return $data;
        }else{
			$querysql = 'SELECT DISTINCT COD_PROG, PROGRAMA FROM UAOWEB.V_MOODLE_CURSO_GRUPO_EST WHERE PERIODO =:periodo ORDER BY COD_PROG ASC';
			$stid = oci_parse($conn, $querysql);
			oci_bind_by_name($stid, ':periodo', $periodo);
			oci_execute($stid);
			
			$programas = array();
			$sw = false;
			
			while (($row = oci_fetch_array($stid, OCI_ASSOC)) != false) {
				$programas[] = array($row['COD_PROG'], $row['PROGRAMA']);
				$sw = true;
			}

			oci_free_statement($stid);
			oci_close($conn);
			
			if ($sw) {
				$data = array("status" => "success", "programas" => $programas);
				
				return $data;
			} else {
				$data = array ("status" => "not-found", "msg" => "no hay registros curso: ".$curso." periodo: ".$periodo);
                return $data;
			}
        }
	}
?>
