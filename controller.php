<?php
    include("log.php");
    include("querys.php");
    // Permite la conexion desde cualquier origen
    header("Access-Control-Allow-Origin: *");
    // Permite la ejecucion de los metodos
    header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE");  

    $array = explode("/", $_SERVER['REQUEST_URI']);
    // Obtener el cuerpo de la solicitud HTTP
    $bodyRequest = file_get_contents("php://input");
    //en caso de solicitar una URL con final / eliminamos esa posicion

    // creamos parametros
	$process = false;
	switch (count($array)) {
		case 6:
			// uri tipo: http://domain/sw/api/recurso/id1/id2
			$id2 = $array[count($array) - 1];
			$id = $array[count($array) - 2];
			$recurso = $array[count($array) - 3];
			$process = true;
			break;
		case 5:
			// uri tipo: http://domain/sw/api/recurso/id1
			$id = $array[count($array) - 1];
			$recurso = $array[count($array) - 2];
			$process = true;
			break;
		case 4:
			// uri tipo: http://domain/sw/api/recurso
			$recurso = $array[count($array) - 1];
			$process = true;
			break;
		default:
			print_json(400, "Bad Request", null);
			break;
	}
    
    if ($process) {
		$tiempo = date('d-m-Y H:i:s');
		$method_request = $_SERVER['REQUEST_METHOD']; 
		$ip_remote = $_SERVER['REMOTE_ADDR'];
		$record_log = $tiempo.' '.$ip_remote.' '.$method_request.' '.$recurso.PHP_EOL;
		save_record_to_log($record_log);        
        
    		 // Analiza el metodo usado actualmente de los cuatro disponibles: GET, POST
		switch ($_SERVER['REQUEST_METHOD']) {
            case 'GET':
                switch($recurso) {
                    case "periodos":
                        $data = getPeriodos();
                        switch($data['status']) {
                            case "success":
                                print_json(200, "OK", $data);
                                break;
                            case "failed":
                                print_json(503, "Service unavailable", $data);
                                break;
                            case "not-found":
                                print_json(404, "Not found", $data);
                                break;

                        }
                        break;

                    case "periodo":
                        $data = getLastPeriodo();
                        switch($data['status']) {
                            case "success":
                                print_json(200, "OK", $data);
                                break;
                            case "failed":
                                print_json(503, "Service unavailable", $data);
                                break;
                            case "not-found":
                                print_json(404, "Not found", $data);
                                break;

                        }
                        break;
                        
                    case "docente":
                        if(isset($id)) {
                            $userup = strtoupper($id);
                            $data = getDocente($userup);
                            switch($data['status']) {
                                case "success":
                                    print_json(200, "OK", $data);
                                    break;
                                case "failed":
                                    print_json(503, "Service unavailable", $data);
                                    break;
                                case "not-found":
                                    print_json(404, "Not found", $data);
                                    break;
                            }
                        } else {
                            print_json(400, "Bad Request", null); 
                        }
                        break;
                        
                    case "asignatura":
                        if(isset($id)) {
                            $data = getAsignatura($id);
                            switch($data['status']) {
                                case "success":
                                    print_json(200, "OK", $data);
                                    break;
                                case "failed":
                                    print_json(503, "Service unavailable", $data);
                                    break;
                                case "not-found":
                                    print_json(404, "Not found", $data);
                                    break;
                            }
                        } else {
                            print_json(400, "Bad Request", null); 
                        }

                        break;

                    case "grupos":
                        if(isset($id) && isset($id2)) {
                            $data = getGrupos($id, $id2);
                            switch($data['status']) {
                                case "success":
                                    print_json(200, "OK", $data);
                                    break;
                                case "failed":
                                    print_json(503, "Service unavailable", $data);
                                    break;
                                case "not-found":
                                    print_json(404, "Not found", $data);
                                    break;
                            }
                        } else {
                            print_json(400, "Bad Request", null); 
                        }
                    break;

                    case "estudiantes":
                        if(isset($id) && isset($id2)) {
                            $data = getEstudiantes($id, $id2);
                            switch($data['status']) {
                                case "success":
                                    print_json(200, "OK", $data);
                                    break;
                                case "failed":
                                    print_json(503, "Service unavailable", $data);
                                    break;
                                case "not-found":
                                    print_json(404, "Not found", $data);
                                    break;
                            }
                        } else {
                            print_json(400, "Bad Request", null); 
                        }
                    break;

					case "programas":
						if(isset($id)) {
							$data = getProgramas($id);
                            switch($data['status']) {
                                case "success":
                                    print_json(200, "OK", $data);
                                    break;
                                case "failed":
                                    print_json(503, "Service unavailable", $data);
                                    break;
                                case "not-found":
                                    print_json(404, "Not found", $data);
                                    break;
                            }
                        } else {
                            print_json(400, "Bad Request", null); 
                        }
		
						break;
                    default:
						print_json(404, "Not found", null);
						break;                        
                }
                break;
            default:
				// Acciones cuando el metodo no se permite
				// En caso de que el Metodo Solicitado no sea ninguno de los cuatro disponible, envia la siguiente respuesta
				print_json(405, "Method Not Allowed", null);
				break;
        }
    }
    
        // Esta funcion imprime las respuesta en estilo JSON y establece los estatus de la cebeceras HTTP
    function print_json($status, $mensaje, $data) {
        header("HTTP/1.1 $status $mensaje");
        header("Content-Type: application/json; charset=UTF-8");
    
        $response['statusCode'] = $status;
        $response['statusMessage'] = $mensaje;
        $response['data'] = $data;
    
        echo json_encode($response, JSON_PRETTY_PRINT);
    }
?>
